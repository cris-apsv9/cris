<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CRIS Publication view</title>
</head>
<body>
<%@ include file="Header.jsp"%>

<c:if test="${user.id == 'root'}">
<h2>Update citations of publication</h2>
<a href="UpdateCitationsAPIServlet?id=${publication.id}"> ${publication.id}</a>
</c:if>

<h2>Publication info</h2>
<table>
  <tr><th>Id</th><th>Title</th><th>Publication name</th>
      <th>Date</th><th>Authors</th><th>Cite count</th></tr>
	<tr>
	    <td>${publication.id}</td>
		<td>${publication.title}</td>
		<td>${publication.publicationName}</td>
		<td>${publication.publicationDate}</td>
		<td>${publication.authors}</td>
		<td>${publication.citeCount}</td>
	</tr>
</table>
</body>
</html>
