package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreateResearcherServlet
 */
@WebServlet("/CreateResearcherServlet")
public class CreateResearcherServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateResearcherServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//es un dopost porque hay que modificar el dao
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//coger user de la sesion
		Researcher user = (Researcher) (request.getSession().getAttribute("user"));
		//ver si el usuario es admin
		if ((user != null) && (user.getId().equals("root"))) {
			//crear el nuevo investigador
			Researcher n = new Researcher();
			n.setId(request.getParameter("id"));
			n.setName(request.getParameter("name"));
			n.setLastname(request.getParameter("lastname"));
			n.setEmail(request.getParameter("email"));
			n.setPassword(request.getParameter("password"));
			n.setScopusURL(request.getParameter("scopusURL"));
			
			Client client = ClientBuilder.newClient(new ClientConfig());
	        client.target(URLHelper.getInstance().getCrisURL() + "/rest/Researchers/").request()
	                .post(Entity.entity(n, MediaType.APPLICATION_JSON), Response.class);
	        response.sendRedirect(request.getContextPath()+"/AdminServlet");
		}
		else {
			request.setAttribute("message", "Invalid user or password");
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
	
	}

}
