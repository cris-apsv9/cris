package es.upm.dit.apsv.cris.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.apsv.cris.model.Publication;
import es.upm.dit.apsv.cris.model.Researcher;

/**
 * Servlet implementation class CreatePublicationServlet
 */
@WebServlet("/CreatePublicationServlet")
public class CreatePublicationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreatePublicationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	//es un dopost porque hay que modificar el dao
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//coger user de la sesion
		Researcher user = (Researcher) (request.getSession().getAttribute("user"));
		//ver si el usuario es admin
		if ((user != null) && (user.getId().equals("root"))) {
			//crear la nueva publicacion
			Publication p = new Publication();
			p.setId(request.getParameter("id"));
			p.setTitle(request.getParameter("title"));
			p.setPublicationName(request.getParameter("publicationname"));
			p.setPublicationDate(request.getParameter("publicationdate"));
			p.setAuthors(request.getParameter("authors"));
			
			Client client = ClientBuilder.newClient(new ClientConfig());
	        client.target(URLHelper.getInstance().getCrisURL() + "/rest/Publications/").request()
	                .post(Entity.entity(p, MediaType.APPLICATION_JSON), Response.class);
	        response.sendRedirect(request.getContextPath()+"/AdminServlet");
		}
		else {
			request.setAttribute("message", "Invalid user or password");
			getServletContext().getRequestDispatcher("/LoginView.jsp").forward(request, response);
		}
	
	}

}
